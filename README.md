graph app
=============================

Graph demo app

Installation
------------

```
    $ python setup.py install
    $ pip uninstall djangorestframework
    $ pip install djangorestframework
    $ python manage.py clearsessions
    $ python manage.py makemigrations
    $ python manage.py migrate
    $ uwsgi --module=configuration.wsgi:application --env DJANGO_SETTINGS_MODULE=configuration.settings --http-socket :8057 -b 32768 --harakiri=20 --max-requests=5000 --vacuum
    $ remember to add this property to /etc/nginx/nginx.conf --> client_max_body_size 10M;

```

Requirements
------------

* Python_ 2.7+ (not tested with lower version)
* Django_ 1.7+ (may work on lower version, but not tested)

.. _Python: https://python.org
.. _Django: http://djangoproject.com


How to use?
-----------

* See documentation: 
* Mailing list: 
