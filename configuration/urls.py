from graph import views
from django.conf import settings
from django.conf.urls import patterns, include, url, static
from django.conf.urls.static import static
from django.contrib import *
from rest_framework.routers import DefaultRouter
from rest_framework.authtoken.views import obtain_auth_token

# Create a router and register our viewsets with it.
router = DefaultRouter()
#router.register(r'child_nodes', views.ChildNodeViewSet)

# The API URLs are now determined automatically by the router.
# Additionally, we include the login URLs for the browseable API.
urlpatterns = patterns('',
    url(r'^child_nodes/(?P<node_id>[0-9]*)/$', views.childNodes),
	url(r'^element/$', views.addElement),
)