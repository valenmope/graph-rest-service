from rest_framework import status
from rest_framework.decorators import *
from rest_framework.response import Response
import logging

logger = logging.getLogger(__name__)

tree_map = {}
tree_map[None] = [{"id": 1, "name": "element-1", "parent_id": None}]
tree_map[1] = [{"id": 2, "name": "element-2", "parent_id": 1}, {"id": 3, "name": "element-3", "parent_id": 1}]
tree_map[2] = [{"id": 4, "name": "element-4", "parent_id": 2}, {"id": 5, "name": "element-5", "parent_id": 2}]
tree_map[3] = [{"id": 6, "name": "element-6", "parent_id": 3}, {"id": 7, "name": "element-7", "parent_id": 3}]

global tree_counter
tree_counter = 8


@api_view(('GET',))
def childNodes(request, node_id):
    logger.debug('GET request')
    data = tree_map.get(int(node_id), [])

    return Response(data)

@api_view(('POST',))
def addElement(request):
    global tree_counter
    name = request.DATA.get('element_name', '')
    parent = request.DATA.get('parent_element', 1)
    parent = int(parent)
    logger.debug(tree_map)

    if parent not in tree_map:
        parent = 1

    tree_map[parent].append({"id": tree_counter, "name": name, "parent_id": parent})
    tree_counter += 1

    return Response(status=status.HTTP_200_OK)
